from pymongo import MongoClient
from conf.settings import APP_SETTINGS, AUTH_SETTINGS

"""
CLIENT INTERFACE

    The database name is often dynamically generated, so it is stored within the app
    instance's "config.ini".

    One standard collection exists across all API instances:
        - AUTH_COLLECTION: Contains records of all users, their passwords, and their sessions
"""


_client = MongoClient("localhost", 27017)
DB = _client[APP_SETTINGS["db_name"]]

AUTH_COLLECTION = DB[AUTH_SETTINGS["auth_document_name"]]
