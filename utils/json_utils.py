import json
from bson.objectid import ObjectId
from datetime import datetime


def return_json(data):
    return json.dumps(data, cls=MongoDocumentEncoder)


def load_json(data):
    return json.loads(data)


class MongoDocumentEncoder(json.JSONEncoder):
    def default(self, obj):
        # Set the object ID as a string
        if isinstance(obj, ObjectId):
            return str(obj)
        # Format date for return
        elif isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)
