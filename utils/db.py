from bson.objectid import ObjectId
import falcon
from utils.json_utils import return_json


# Read and return a specific record from the given collection
def return_single(collection, uid):
    try:
        obj_id = ObjectId(uid)
    except Exception as ex:
        raise falcon.HTTPError(falcon.HTTP_400, "Error", ex.message)

    query = collection.find_one({"_id": obj_id})
    return (return_json(query), falcon.HTTP_200)


# Read all records in given collection
def return_array(collection):
    query = collection.find()
    return (return_json(list(query)), falcon.HTTP_200)
