from conf.settings import AUTH_SETTINGS
from talons.auth import basicauth, middleware, external

auth_config = {
    "delay_403": True,
    "external_authn_callable": AUTH_SETTINGS["external_authn_callable"]
}

auth_middleware = middleware.create_middleware(
    identify_with=basicauth.Identifier,
    authenticate_with=external.Authenticator,
    **auth_config
)
