from db.client import DB
from conf.settings import AUTH_SETTINGS
import falcon


def connect_to_collection(req, resp, params):
    """
    Dynamically connect to a connection

    If user attempts to connect to the auth collection then reject
    them with a 403
    """
    if params["collection"] == AUTH_SETTINGS["auth_document_name"]:
        raise falcon.HTTPError(falcon.HTTP_403, "Error")
    params["this_collection"] = DB[params["collection"]]
