import sys
import getopt
from passlib.hash import sha256_crypt
from db.client import AUTH_COLLECTION

"""
NEW_USER
    Tools and function to generate new user-password pair
"""


def _get_command_args(args):
    """
    Return username and password extracted from the command line
    that will be used to create new authorised account.
    """
    username, password = None, None

    def _raise_error(msg=None):
        """
        Handle errors in the input process
        """
        if msg:
            print msg
        print "\nCorrect usage:"
        print "new_user.py -u [username] -p [password]\n"
        sys.exit()

    try:
        opts, args = getopt.getopt(args, "hu:p:")
    except getopt.GetoptError:
        _raise_error("Unable to extract arguments.\n")

    if not opts:
        _raise_error("Please provide username and password combination.\n")

    for opt, arg in opts:
        if opt == "-h":
            _raise_error()
        elif opt == "-u":
            username = arg
        elif opt == "-p":
            password = arg
        else:
            _raise_error("Incorrect args passed in.\n")

    if not username or not password:
        _raise_error("Please supply both password and username.\n")

    return username, password


def add_user(credentials):
    """
    Add a new user using dict suppled, that contains username and password
    """
    # Check if a user with that username already exists
    if not list(AUTH_COLLECTION.find({"username": credentials["username"]}).limit(1)):
        hash = sha256_crypt.encrypt(credentials["password"])
        print "User requires creating"
        AUTH_COLLECTION.insert({
            "username": credentials["username"],
            "passw": hash
        })
    else:
        print "User with that username already exists"


if __name__ == "__main__":
    username, password = _get_command_args(sys.argv[1:])
    add_user({
        "username": username,
        "password": password
    })
