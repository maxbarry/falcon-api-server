from conf.settings import AUTH_SETTINGS
import tokenlib

"""
TOKEN
    Manager that handles generation of time based tokens

    Token secret and length (in seconds) are specified in the external
    config file
"""

manager = tokenlib.TokenManager(secret=AUTH_SETTINGS["token_secret"], timeout=int(AUTH_SETTINGS["token_timeout"]))
