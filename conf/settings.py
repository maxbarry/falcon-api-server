from ConfigParser import ConfigParser

"""
APP SETTINGS

    Dynamic settings are initialised from "config.ini" located
    in the same directory

    Each section of "config.ini" is responsible for a part of the app:
         - Authorization: Auth settings including app secret and authenticator location
         - General: App-wide settings

    Connection to the client DB is also made and stored as "client"

"""

# Config.ini init
_config = ConfigParser()
_config.read("conf/config.ini")

AUTH_SETTINGS = dict(_config.items("Authorization"))
APP_SETTINGS = dict(_config.items("General"))
