from passlib.hash import sha256_crypt
from db.client import AUTH_COLLECTION

"""
AUTHENTICATOR

    Falcon returns an identity object containing supplied username
    ("identity.login") and supplied password ("identity.key").

    The authenticator first checks if a user with that username exists.
    Then it checks that if the user does exist, their password validates against
    that which was supplied.

    A boolean is returned, and access is permitted to an endpoint based on
    this value.
"""


def authenticate(identity):

    # Grab the user object
    user = list(AUTH_COLLECTION.find({
        "username": identity.login
    }).limit(1))

    # If no user with that username can be found, return False for auth
    if not user:
        return False

    # Verify hash against provided password
    return sha256_crypt.verify(identity.key, user[0]["passw"])
