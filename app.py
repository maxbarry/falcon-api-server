# import json
# import base64
# from uuid import uuid4
# from datetime import datetime
from wsgiref import simple_server
# from bson.objectid import ObjectId
import falcon
# from pymongo import MongoClient
# import tokenlib
# from utils.db import return_single, return_array
# from utils.json import return_json
# from db.middleware import connect_to_collection
from auth.auth_class import Auth
from app.api_class import ApiServer
# from auth.middleware import auth_middleware

# Mongo setup
# connection = MongoClient("localhost", 27017)
# Todo: Move DB name to settings
# db = connection["APIServer"]


# API SETTINGS
# auth_collection_name = "test_adminusers"


# HOOKS
def headers_for_all(req, resp, params):
    resp.set_headers({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, x-auth-user, x-auth-password, Authorization',
        'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE'
    })


# def connect_to_collection(req, resp, params):
#     if params["collection"] == auth_collection_name:
#         raise falcon.HTTPError(falcon.HTTP_403, "Error")
#     params["this_collection"] = db[params["collection"]]


# UTILS
# def return_json(data):
#     return json.dumps(data, cls=MongoDocumentEncoder)


# def load_json(data):
#     return json.loads(data)


# Read and return a specific record from the given collection
# def return_single(collection, uid):
#     try:
#         obj_id = ObjectId(uid)
#     except Exception as ex:
#         raise falcon.HTTPError(falcon.HTTP_400, "Error", ex.message)

#     query = collection.find_one({"_id": obj_id})
#     return (return_json(query), falcon.HTTP_200)


# # Read all records in given collection
# def return_array(collection):
#     query = collection.find()
#     return (return_json(list(query)), falcon.HTTP_200)


# Properly encodes Mongo records for JSON
# class MongoDocumentEncoder(json.JSONEncoder):
#     def default(self, obj):
#         # Set the object ID as a string
#         if isinstance(obj, ObjectId):
#             return str(obj)
#         # Format date for return
#         elif isinstance(obj, datetime):
#             return obj.isoformat()
#         return json.JSONEncoder.default(self, obj)


# AUTH

# auth_config = {
#     "external_authn_callable": "auth.authenticator.authenticate",
#     "delay_403": True
# }

# manager = tokenlib.TokenManager(secret="pMRUq67Kfx", timeout=4)


# token_config = {
#     "external_authn_callable": "auth.ab.token_authenticate",
#     "delay_401": True,
#     "delay_403": True
# }

# token_middleware = middleware.create_middleware(identify_with=basicauth.Identifier, authenticate_with=external.Authenticator, **token_config)


# class AuthServer(object):
#     """
#     Class handling storing new Oauth users that admins can then grant
#     write privileges for the API
#     """
#     def __init__(self):
#         super(AuthServer, self).__init__()

#     def _create_session(self, username):
#         session_id = str(uuid4()) + str(uuid4())
#         session_token = manager.make_token({"session_id": session_id})
#         self.auth_document.update({"username": username}, {"$set": {"session": session_id, "token": session_token}})
#         return falcon.HTTP_200, return_json({"session": {"id": session_id, "token": session_token}})

#     def on_options(self, req, resp):
#         resp.status = falcon.HTTP_200

#     @falcon.before(auth_middleware)
#     def on_get(self, req, resp):
#         self.auth_document = db[auth_collection_name]

#         # DATA = json.load(req.stream, 'utf-8')
#         print "@@@@@@@@"
#         base_auth = req.auth.split()[1]
#         username = base64.b64decode(base_auth).split(":")[0]
#         print username
#         user_obj = list(self.auth_document.find({"username": username}).limit(1))
#         # Check if session already exists and create one if it doesn't
#         print user_obj
#         print "@@@@@@@@"

#         if user_obj and not "session" in user_obj[0]:
#             resp.status, resp.data = self._create_session(username)
#         else:
#             try:
#                 # TODO : You want to create multiple sessions from different locations
#                 print "trying to check token validity"
#                 existing_token = str(user_obj[0]["token"])
#                 print existing_token
#                 manager.parse_token(existing_token)
#                 print "Parsing existing token and it is fine"
#                 resp.status = falcon.HTTP_409
#                 resp.data = return_json({"error": "Session already validated", "message": "Session already exists for that user"})
#             except tokenlib.errors.ExpiredTokenError:
#                 print "Token has expired!!!!"
#                 # Token is expired, therefore recreate session
#                 resp.status, resp.data = self._create_session(username)

#         # If session is passed through you know it is a login attempt
#         # Authorize the user against a store with talons.
#         # Create a session and an access token that expires in 15 days.
#         # If the user has the access token AND access token is valid on future submissions, let them submit
#         # Else return false for login and force them to log in.
#         # Create someway to clear dead access tokens/revoke access token command.
#         # If not "session" as key then it must be an access token attempt. In which case just auth them.


# CORE API CLASS
# @falcon.before(connect_to_collection)
# class ApiServer(object):
#     """Core class for handling requests to the API server"""

#     def __init__(self):
#         super(ApiServer, self).__init__()

#     def on_get(self, req, resp, this_collection, collection, uid=False):
#         '''
#         Handles all GET requests.
#         Follows REST standard:
#             -  If UID is part of URL, a single object is returned
#             -  If no UID, all objects are returned
#         '''
#         if uid:
#             # if uid return_single...
#             resp.data, resp.status = return_single(this_collection, uid)
#         else:
#             # .. else return_array of all records
#             resp.data, resp.status = return_array(this_collection)

#     def on_options(self, req, resp, this_collection, collection, uid=False):
#         '''
#         Handles all OPTIONS requests.
#         Returns status code 200.
#         '''
#         resp.status = falcon.HTTP_200

#     def on_post(self, req, resp, this_collection, collection, uid=False):
#         '''
#         Handles all POST requests. POST requests add new objects to the database.
#         Expected input is a single object or array of objects.
#         '''
#         # Store POST'ed JSON as DATA

#         print "token authenticate"
#         print req.auth

#         bearer_auth = req.auth.split()[1]
#         token = base64.b64decode(bearer_auth).split(":")[1]

#         print token

#         try:
#             if tokenlib.parse_token(token, secret="pMRUq67Kfx"):
#                 pass
#             else:
#                 resp.data, resp.status = return_json({"error": "Unauthorized", "message": "Please authenticate"}), falcon.HTTP_403
#                 return
#         except tokenlib.errors.ExpiredTokenError:
#             resp.data, resp.status = return_json({"error": "Session expired", "message": "Session expired"}), falcon.HTTP_403
#             # TODO : Unset the token
#             return
#         except:
#             resp.data, resp.status = return_json({"error": "Unauthorized", "message": "Please authenticate"}), falcon.HTTP_403
#             return

#         DATA = json.load(req.stream, 'utf-8')
#         if isinstance(DATA, list):
#             # If multiple items have been posted, loop over them
#             responseBody = []
#             for item in DATA:
#                 # Add item to collection
#                 newObject = this_collection.insert(DATA)
#                 # Append to the body response the object of this new item
#                 responseBody.append(this_collection.find_one({"_id": newObject}))
#         else:
#             # Insert single object
#             newObject = this_collection.insert(DATA)
#             # Return that newly created object as the response
#             responseBody, resp.status = return_single(this_collection, newObject)

#         resp.data, resp.status = (responseBody, falcon.HTTP_201)

#     def on_put(self, req, resp, this_collection, collection, uid):
#         '''
#         Handles all PUT requests. PUT requests modify individual objects in the database.
#         Expected input is a single object PUT to UID.
#         The object will be the entire object as found in database, with modifications.
#         '''
#         DATA = json.load(req.stream, 'utf-8')
#         updated = this_collection.find_and_modify({"_id": ObjectId(uid)}, {"$set": DATA}, new=True)

#         resp.data, resp.status = (return_json(updated), falcon.HTTP_200)

#     def on_delete(self, req, resp, this_collection, collection, uid):
#         '''
#         Handles all OPTIONS requests.
#         Returns status code 200.
#         '''
#         try:
#             obj_id = ObjectId(uid)
#         except Exception as ex:
#             raise falcon.HTTPError(falcon.HTTP_400, "Error", ex.message)
#         obj_to_delete = this_collection.find_one({"_id": obj_id})
#         if obj_to_delete:
#             this_collection.remove({"_id": obj_id})
#             resp.data, resp.status = (return_json({}), falcon.HTTP_200)
#         else:
#             raise falcon.HTTPError(falcon.HTTP_400, "Error", "Could not find the object to delete")


# CLASS INSTANTIATIONS
wsgi_app = app = falcon.API(before=[headers_for_all])
core_api = ApiServer()
auth_api = Auth()

# ROUTES
app.add_route("/o/", auth_api)
app.add_route("/v1/{collection}", core_api)
app.add_route("/v1/{collection}/{uid}", core_api)

# DEBUG SERVER
if __name__ == '__main__':
    httpd = simple_server.make_server('127.0.0.1', 8000, app)
    httpd.serve_forever()
