import json
import base64
from bson.objectid import ObjectId
import tokenlib
import falcon
from utils.db import return_single, return_array
from utils.json_utils import return_json
from db.middleware import connect_to_collection
from auth.token import manager


@falcon.before(connect_to_collection)
class ApiServer(object):
    """
    Handles requests to the API server
    """

    def __init__(self):
        super(ApiServer, self).__init__()

    @staticmethod
    def _is_valid_token(req):
        """
        Request is checked for valid session token in authorization header
        """

        bearer_auth = req.auth.split()[1]
        token = base64.b64decode(bearer_auth).split(":")[1]

        try:
            if manager.parse_token(token):
                return True, None, None
            else:
                return False, falcon.HTTP_403, {"error": "Unauthorized", "message": "Please authenticate"}

        except tokenlib.errors.ExpiredTokenError:
            return False, falcon.HTTP_403, {"error": "Session expired", "message": "Session expired"}

        except:
            return False, falcon.HTTP_403, {"error": "Unauthorized", "message": "Please authenticate"}

    def on_get(self, req, resp, this_collection, collection, uid=False):
        '''
        Handles all GET requests.
        Follows REST standard:
            -  If UID is part of URL, a single object is returned
            -  If no UID, all objects for that collection are returned
        '''
        if uid:
            # if uid return_single...
            resp.data, resp.status = return_single(this_collection, uid)
        else:
            # .. else return_array of all records
            resp.data, resp.status = return_array(this_collection)

    def on_options(self, req, resp, this_collection, collection, uid=False):
        '''
        Handles all OPTIONS requests.
        Returns status code 200.
        '''
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp, this_collection, collection, uid=False):
        '''
        Handles all POST requests. POST requests add new objects to the database.
        Expected input is a single object or array of objects.

        First checks if a valid token has been supplied with the request
        If not, call returns with error code and message
        '''

        valid_session, err_code, err_msg = self._is_valid_token(req)

        if not valid_session:
            resp.status, resp.data = err_code, err_msg
            return

        DATA = json.load(req.stream, 'utf-8')

        """
        If supplied data is an array, then items are looped over
        and inserted in to the collection individually
        """
        if isinstance(DATA, list):
            response_body = []
            for item in DATA:
                # Add item to collection
                newObject = this_collection.insert(DATA)
                # Append to the body response the object of this new item
                response_body.append(this_collection.find_one({"_id": newObject}))
        else:
            # Insert single object
            newObject = this_collection.insert(DATA)
            # Return that newly created object as the response
            response_body, resp.status = return_single(this_collection, newObject)

        resp.data, resp.status = response_body, falcon.HTTP_201

    def on_put(self, req, resp, this_collection, collection, uid):
        '''
        Handles all PUT requests. PUT requests modify individual objects in the database.
        Expected input is a single object PUT to UID.
        The object will be the entire object as found in database, with modifications.
        '''
        DATA = json.load(req.stream, 'utf-8')
        updated = this_collection.find_and_modify({"_id": ObjectId(uid)}, {"$set": DATA}, new=True)

        resp.data, resp.status = (return_json(updated), falcon.HTTP_200)

    def on_delete(self, req, resp, this_collection, collection, uid):
        '''
        Handles all OPTIONS requests.
        Returns status code 200.
        '''
        try:
            obj_id = ObjectId(uid)
        except Exception as ex:
            raise falcon.HTTPError(falcon.HTTP_400, "Error", ex.message)
        obj_to_delete = this_collection.find_one({"_id": obj_id})
        if obj_to_delete:
            this_collection.remove({"_id": obj_id})
            resp.data, resp.status = (return_json({}), falcon.HTTP_200)
        else:
            raise falcon.HTTPError(falcon.HTTP_400, "Error", "Could not find the object to delete")
