from uuid import uuid4
import base64
import falcon
import tokenlib
from auth.middleware import auth_middleware
from auth.token import manager
from utils.json_utils import return_json
from db.client import AUTH_COLLECTION


class Auth(object):
    """
    GET to /o/ assigns a session token and id to a user. This token grants the user
    write access to the API for the length of that session.

    To be granted a session token user must supply a valid username and password
    in the authorization header of the GET.

    The authentication of these details are verified by the auth_middleware
    """
    def __init__(self):
        super(Auth, self).__init__()

    @staticmethod
    def _create_session(user):
        """
        Create session id using random strings
        Create session token using secret and timeout from manager
        Store these session values against the user in the DB
        """
        session_id = str(uuid4()) + str(uuid4())
        session_token = manager.make_token({"session_id": session_id})

        AUTH_COLLECTION.update({"username": user["username"]}, {"$set": {"session": session_id, "token": session_token}})

        return falcon.HTTP_200, return_json({
            "session": {"id": session_id, "token": session_token}
        })

    @staticmethod
    def _has_existing_session(user):
        """
        Check if the user exists
        Check if the user already has a session
        Check if that session is valid

        If no valid existing session exists, return False and create
        and create a session
        """
        if user and not "session" in user:
            return False, None, None

        try:
            # TODO : You want to create multiple sessions from different locations
            existing_token = str(user["token"])
            manager.parse_token(existing_token)

            err_msg = {
                "error": "Session already validated",
                "message": "Session already exists for that user"
            }
            return True, falcon.HTTP_409, err_msg

        except tokenlib.errors.ExpiredTokenError:
            # Token has expired, therefore new session required
            return False, None, None

    @staticmethod
    def _return_user_object(req):
        """
        Get the user credentials from the authorization header that
        middleware has previously validated
        """
        base_auth = req.auth.split()[1]
        username = base64.b64decode(base_auth).split(":")[0]
        return list(AUTH_COLLECTION.find({"username": username}).limit(1))[0]

    def on_options(self, req, resp):
        resp.status = falcon.HTTP_200

    @falcon.before(auth_middleware)
    def on_get(self, req, resp):
        """
        Check if the user has an existing session, and if not create
        a token session
        """
        user = self._return_user_object(req)
        session, err_code, err_msg = self._has_existing_session(user)

        if not session:
            resp.status, resp.data = self._create_session(user)
        else:
            resp.status, resp.data = err_code, return_json(err_msg)

        # TODO : Create someway to clear dead access tokens/revoke access token command
